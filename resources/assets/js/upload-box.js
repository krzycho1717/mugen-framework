/**
 * Created by Krzysztof Stec on 10.06.2017.
 */
var handlebars = require('handlebars');

const emptyTemplate = handlebars.compile(`
    <h4 class="ui header">
        <div class="sub header">brak</div>
    </h4>
`, null);
const fileTemplate = handlebars.compile(`
    <div class="item" data-filename="{{filename}}">
        <div class="ui tiny image">
            <img src="{{img}}" alt=""/>
        </div>
        <div class="content">
            <div class="header">{{short_filename}}</div>
            <div class="right floated remove-file">
                <i class="remove icon"></i>
            </div>
            <div class="meta clearfix">
                <div class="ui teal tiny progress" data-percent="0" id="example1">
                    <div class="bar"></div>
                    <!--<div class="label">0% Funded</div>-->
                </div>
            </div>
            <div class="extra">
                <div class="left floated">0%</div>
                <div class="right floated">0/{{size}}</div>
            </div>
        </div>
        <div class="ui section divider"></div>
    </div>
`, null);

( function ( document, window, index )
{
    // feature detection for drag&drop upload
    var isAdvancedUpload = function()
    {
        var div = document.createElement( 'div' );
        return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
    }();


    // applying the effect for every form
    var boxes = document.querySelectorAll( '.box' );
    Array.prototype.forEach.call( boxes, function( box ) {
        var form         = box.querySelector( 'form' ),
            reload_box   = false,
            input        = box.querySelector( 'input[type="file"]' ),
            label        = box.querySelector( '.box__input label' ),
            errorMsg     = box.querySelector( '.box__error span' ),
            restart      = box.querySelectorAll( '.box__restart' ),
            preview      = document.querySelector( '.box__preview' ),
            button       = document.querySelector( '.box__button' ),
            droppedFiles = false,
            fileNumber   = 0,
            deleteFile = function (e)
            {
                var it = e.target.parentNode.parentNode.parentNode;
                if(it){
                    var fn = it.dataset.filename;
                    //console.info('Deleting \'%s\'', fn);
                    //console.info('File count: %d', (droppedFiles)? droppedFiles.length : 0);
                    Array.prototype.forEach.call(droppedFiles, function ( file, index, all ) {
                        if(file.name == fn){
                            //console.dir(all);
                            all.splice(index, 1);
                            it.parentNode.removeChild(it);
                        }
                    });
                }
            },
            bytesToSize = function( bytes ) {
               var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
               if (bytes == 0) return '0 Byte';
               var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
               return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            },
            truncateFileName = function(n, len) {
                  var ext = n.substring(n.lastIndexOf(".") + 1, n.length).toLowerCase();
                  var filename = n.replace('.'+ext,'');
                  if(filename.length <= len) {
                      return n;
                  }
                  filename = filename.substr(0, len) + (n.length > len ? '[...]' : '');
                  return filename + '.' + ext;
            },
            showFiles    = function( files )
            {
                fileNumber = files.length;
                if(fileNumber == 0) {
                    button.disabled = true;
                    preview.innerHTML = emptyTemplate();
                    return;
                }
                button.disabled = false;
                Array.prototype.forEach.call( files, function( file, index, all ) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        preview.insertAdjacentHTML('beforeend', fileTemplate({
                            img: e.target.result,
                            filename: file.name,
                            short_filename: truncateFileName(file.name, 16),
                            size: bytesToSize(file.size),
                        }));
                        Array.prototype.forEach.call( preview.querySelectorAll('.remove-file'), function ( button, index ) {
                            //console.info('button #%d', index);
                            button.addEventListener('click', deleteFile);
                        });
                        if(index == all.length -1)
                            $('#loader').hide();
                    }
                    reader.readAsDataURL(file);
                });
            };

        // automatically submit the form on file select
        input.addEventListener( 'change', function( e ) {
            $('#loader').show();
            preview.innerHTML = '';
            //droppedFiles = e.target.files;
            droppedFiles = [];
            Array.prototype.forEach.call(e.target.files, function ( file ) {
                droppedFiles.push(file);
            });
            showFiles( e.target.files );
        });

        // drag&drop files if the feature is available
        if( isAdvancedUpload ) {
            box.classList.add( 'has-advanced-upload' ); // letting the CSS part to know drag&drop is supported by the browser

            [ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event )
            {
                form.addEventListener( event, function( e )
                {
                    // preventing the unwanted behaviours
                    e.preventDefault();
                    e.stopPropagation();
                });
            });
            [ 'dragover', 'dragenter' ].forEach( function( event )
            {
                form.addEventListener( event, function()
                {
                    box.classList.add( 'is-dragover' );
                });
            });
            [ 'dragleave', 'dragend', 'drop' ].forEach( function( event )
            {
                form.addEventListener( event, function()
                {
                    box.classList.remove( 'is-dragover' );
                });
            });
            form.addEventListener( 'drop', function( e ) {
                $('#loader').show();
                preview.innerHTML = '';
                //droppedFiles = e.dataTransfer.files; // the files that were dropped
                droppedFiles = [];
                Array.prototype.forEach.call(e.dataTransfer.files, function ( file, index, all ) {
                    droppedFiles.push(file);
                });
                showFiles( droppedFiles );
            });
        }


        // if the form was submitted
        button.addEventListener( 'click', function( e )
        {
            //console.info('submit');
            //console.dir(droppedFiles);
            //console.info('File count: %d', (droppedFiles)? droppedFiles.length : 0);
            // preventing the duplicate submissions if the current one is in progress
            if( form.classList.contains( 'is-uploading' ) ) return false;

            form.classList.add( 'is-uploading' );
            form.classList.remove( 'is-error' );

            if( isAdvancedUpload ) // ajax file upload for modern browsers
            {
                e.preventDefault();

                if (fileNumber == 0){
                    form.classList.remove('is-uploading');
                    form.classList.add('is-error');
                    errorMsg.textContent = 'No photos selected!';
                }else {
                    if (box.dataset.reload != null)
                        reload_box = true;

                    if( droppedFiles )
                    {
                        Array.prototype.forEach.call( droppedFiles, function( file, index, all )
                        {
                            var ajaxData = new FormData();
                            ajaxData.append( input.getAttribute( 'name' ), file );

                            // ajax request
                            var ajax = new XMLHttpRequest();
                            ajax.upload.addEventListener('progress', function(e){
                                var loaded = e.loaded;
                                var total = e.total;
                                var percentComplete = Math.round((e.loaded / e.total) * 100);
                                $('.item[data-filename="'+file.name+'"] .progress').progress({
                                        percent: percentComplete
                                });
                            });
                            ajax.open(form.getAttribute('method'), form.getAttribute('action'), true);
                            ajax.onload = function () {
                                form.classList.remove('is-uploading');

                                if (ajax.status >= 200 && ajax.status < 400) {
                                    var response = JSON.parse(ajax.responseText);
                                    console.dir(response);
                                    if (response.error){
                                        errorMsg.textContent = response.error;
                                        form.classList.add('is-error');
                                    }else{
                                        form.classList.add('is-success');
                                        if(response.data != null && reload_box){
                                            $("." + box.dataset.reload).html(response.data).find('.image-loading').slideUp();
                                        }

                                        if(index == all.length-1){
                                            $('#uploadCallbackModal').modal("setting", {
                                                closable: true,
                                                onHide: function () {
                                                    preview.innerHTML = emptyTemplate();
                                                    droppedFiles = false;
                                                    button.disabled = true;
                                                    return true;
                                                }
                                            }).modal('show');
                                        }
                                    }
                                }
                                else {
                                    form.classList.add('is-error');
                                    errorMsg.textContent = 'Error. Please, contact the webmaster!';
                                }
                            };

                            ajax.onerror = function () {
                                form.classList.remove('is-uploading');
                                alert('Error. Please, try again!');
                            };
                            ajax.send(ajaxData);
                        });
                    }
                }
                return false;
            }
        });


        // restart the form if has a state of error/success
        Array.prototype.forEach.call( restart, function( entry )
        {
            entry.addEventListener( 'click', function( e )
            {
                preview.innerHTML = emptyTemplate();
            });
        });

        // Firefox focus bug fix for file input
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });

    });
}( document, window, 0 ));