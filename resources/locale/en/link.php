<?php

return [
    'home' => [
      'text' => 'start',
      'desc' => 'where it all begins'
    ],
    'about_us' => [
        'text' => 'about us',
        'desc' => 'poznaj nas'
    ],
    'calendar' => [
        'text' => 'timetable',
        'desc' => 'departure dates'
    ],
    'price' => [
        'text' => 'pricing',
        'desc' => 'no one can beat our prices?'
    ],
    'contact' => [
        'text' => 'contact',
        'desc' => 'have a question??'
    ],
    'rules' => 'rules'

];