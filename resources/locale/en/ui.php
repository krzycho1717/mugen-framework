<?php

return [
    'home' => [
        'title' => 'Home',
        'header' => 'Kupuj w polsce przez internet, wyślij na nasz adres, a my Ci to przywieziemy!'
    ],
    'about_us' => [
        'title' => 'About us',
        'header' => 'Dojedziemy i na koniec świata'
    ],
    'calendar' => [
        'title' => 'Timetable',
        'header' => 'Always on time..'
    ],
    'price' => [
        'title' => 'Prices',
        'header' => 'Simply, cheaply, safely..'
    ],
    'contact' => [
        'title' => 'Contact',
        'header' => 'Let\'s be in touch!'
    ],
];