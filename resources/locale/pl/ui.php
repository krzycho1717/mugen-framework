<?php

return [
    'home' => [
        'title' => 'Home',
        'header' => 'Kupuj w polsce przez internet, wyślij na nasz adres, a my Ci to przywieziemy!'
    ],
    'about_us' => [
        'title' => 'O nas',
        'header' => 'Dojedziemy i na koniec świata'
    ],
    'calendar' => [
        'title' => 'Terminarz',
        'header' => 'Zawsze na czas..'
    ],
    'price' => [
        'title' => 'Cennik',
        'header' => 'Prosto, tanio, bezpiecznie..'
    ],
    'contact' => [
        'title' => 'Kontakt',
        'header' => 'Bądźmy w kontakcie!'
    ],
];