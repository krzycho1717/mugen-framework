<?php

return [
    'home' => [
      'text' => 'główna',
      'desc' => 'wszystko zaczyna się tutaj'
    ],
    'about_us' => [
        'text' => 'o nas',
        'desc' => 'poznaj nas'
    ],
    'calendar' => [
        'text' => 'terminarz',
        'desc' => 'zobacz daty wyjazdów'
    ],
    'price' => [
        'text' => 'cennik',
        'desc' => 'taniej być nie może'
    ],
    'contact' => [
        'text' => 'kontakt',
        'desc' => 'masz pytanie?'
    ],
    'rules' => 'regulamin'

];