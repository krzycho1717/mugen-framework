@extends('layouts.default')

@section('title', 'Upload file')

@section('content')
    <div class="ui container" style="margin-top: 5%">
        <div class="ui three column grid">
            <div class="column"></div>
            <div class="column">
                <form class="ui form" method="post" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    <div class="field required">
                        <label>Plik</label>
                        {{--<input type="file" name="file[]" multiple/>--}}
                        <input type="text" name="test"/>
                    </div>
                    <button class="ui button" type="submit">Submit</button>
                </form>
            </div>
            <div class="column"></div>
        </div>
        <div class="ui list">
            @foreach($users as $user)
                <div class="item">
                    <div class="content">
                        <a class="header">{{ $user->name }} {{ $user->surname }}</a>
                        <div class="description">{{ $user->email }}</div>
                    </div>
                </div>
            @endforeach
        </div>
        <script type="text/javascript">
        $('.teal.button')
      .popup({
        on: 'click'
      });
    $('input')
      .popup({
        on: 'focus'
      });</script>
        <div class="ui teal button" data-title="Using click events" data-content="Clicked popups will close if you click away, but not if you click inside the popup">Click Me</div>
        <div class="ui icon input">
          <input type="text" placeholder="Focus me..." data-content="You can use me to enter data">
          <i class="search icon"></i>
        </div>
    </div>
@stop