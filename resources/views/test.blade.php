@extends('layouts.default')

@section('title', Lang::get('ui.about_us.title'))

@section('content')
    <div id="uploadCallbackModal" class="ui basic modal">
        <div class="ui icon header">
            <i class="file outline icon"></i>
            Sukces
        </div>
        <div class="content" style="text-align: center">
            <h3>Pomyślnie załadowano zdjęcia!</h3>
        </div>
        <div class="actions" style="text-align: center">
            <div class="ui green ok inverted button">
                <i class="checkmark icon"></i>
                Ok
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="css/upload-box.css">
    <div class="ui container" style="margin-top: 5%;margin-bottom: 15px">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="box has-advanced-upload">
                        <form method="post" action="http://mugen.local/file" enctype="multipart/form-data">
                            <div class="box__input">
                                <svg class="box__icon" xmlns="http://www.w3.org/2000/svg" width="50" height="43" viewBox="0 0 50 43">
                                    <path d="M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z"></path>
                                </svg>
                                <input type="file" name="file[]" id="file" class="box__file"
                                       data-multiple-caption="{count} files selected" multiple="">
                                <label for="file"><strong>Wybierz pliki</strong><span class="box__dragndrop"> lub upuść je tutaj</span>.</label>
                                {{--<button class="box__button" disabled>Upload</button>--}}
                            </div>
                            <div class="box__uploading">Uploading…</div>
                            <div class="box__success">Done! <a href="" class="box__restart" role="button">Upload more?</a></div>
                            <div class="box__error">Error! <span></span>. <a href="" class="box__restart" role="button">Try again!</a></div>
                        </form>
                    </div>
                </div>
                <div class="eight wide column" style="box-shadow: -1px 0px 0px 0px #D4D4D5;">
                    <h4 class="ui header" id="file-count">Pliki</h4>
                    <div id="loader" class="ui active inverted dimmer" style="display: none;height: 100px">
                        <div class="ui text loader">Ładowanie</div>
                    </div>
                    <div class="box__preview ui items">
                        <h4 class="ui header">
                            <div class="sub header">brak</div>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="sixteen wide column">
                    <button class="blue ui button right floated box__button" disabled>wyślij</button>
                </div>
            </div>
        </div>
    </div>
    <script src="js/upload-box.js"></script>
    <script type="text/javascript">
        $('#example1').progress();
    </script>
@stop