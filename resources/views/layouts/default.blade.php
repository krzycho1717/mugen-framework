<!DOCTYPE html>
<head>
@include('layouts.header')
</head>
<body class="pusher">

@include('layouts.nav')

<div class="pusher">
    @yield('content')
</div>

@include('layouts.footer')