@extends('layouts.default')

@section('title', Lang::get('ui.about_us.title'))

@section('content')

    <div id="baner" class="ui inverted vertical masthead center aligned segment">

        <div class="ui container">
            <div class="ui large secondary inverted menu">
                <a class="toc item">
                    <i class="sidebar icon"></i>
                </a>
                <a class="active item">Start</a>
                <a class="item">Firma</a>
                <a class="item">Oferta</a>
                <a class="item">Aktualności</a>
                <a class="item">Detal</a>
                <a class="item">HoReCa</a>
                <a class="item">Kontakt</a>
                <div class="right item">
                    <a class="ui inverted button">Log in</a>
                    <a class="ui inverted button">Sign Up</a>
                </div>
            </div>
        </div>

        <div id="main-header" class="ui text container">
            <h1 class="ui inverted header">
                <div class="content">
                    LAPS
                    <div class="sub header" style="margin-top: -18px;margin-left: 70px">spólka z o.o</div>
                </div>
                <h2>Dystrybucja ryb i owoców morza</h2>
            </h1>
        </div>

    </div>
    {{--<div class="ui vertical stripe segment">--}}
        {{--<div class="ui stackable grid container">--}}
            {{--<div class="row">--}}
                {{--<div class="eight wide column">--}}
                    {{--<h3 class="ui header">Serdecznie Witamy !</h3>--}}
                    {{--<p>Firma LAPS sp z o.o. istnieje od 1992 roku. Od początku działalności specjalizujemy się w--}}
                        {{--sprzedaży ryb i owoców morza, jesteśmy pionierem w wprowadzaniu owoców morza na rynek--}}
                        {{--Podkarpacki. W ciągu tych lat osiągnęliśmy wysoką pozycję na rynku dzięki partnerskiej--}}
                        {{--współpracy z Producentami i Klientami. Oferta asortymentowa obejmuje ponad 4000 pozycji--}}
                        {{--asortymentowych. W 2002 wyodrębniono dział HoReCa, a w 2006 r. nastąpiło otwarcie oddziału w--}}
                        {{--Nowym Sączu. Zasięg działania to województwa: podkarpackie, świętokrzyskie, małopolskie,--}}
                        {{--lubelskie. Nasza konkurencyjność wynika z elastyczności dostosowanej do wymagań stawianych nam--}}
                        {{--przez klientów. </p>--}}
                    {{--<h3 class="ui header">We Make Bananas That Can Dance</h3>--}}
                    {{--<p>Yes that's right, you thought it was the stuff of dreams, but even bananas can be--}}
                        {{--bioengineered.</p>--}}
                {{--</div>--}}
                {{--<div class="six wide right floated column">--}}
                    {{--<h3 class="ui header">Obszar naszej działalności</h3>--}}
                    {{--<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"--}}
                            {{--codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0"--}}
                            {{--width="293" height="297" title="">--}}
                        {{--<param name="movie" value="mapa.swf">--}}
                        {{--<param name="quality" value="high">--}}
                        {{--<param name="wmode" value="transparent">--}}
                        {{--<embed src="http://www.laps.pl/mapa.swf" quality="high"--}}
                               {{--pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"--}}
                               {{--type="application/x-shockwave-flash" wmode="transparent" width="293" height="297">--}}
                    {{--</object>--}}
                    {{--<img src="https://semantic-ui.com/examples/assets/images/wireframe/white-image.png" class="ui large bordered rounded image">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="center aligned column">--}}
                    {{--<a class="ui huge button">Check Them Out</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="ui vertical stripe quote segment">--}}
        {{--<div class="ui equal width stackable internally celled grid">--}}
            {{--<div class="center aligned row">--}}
                {{--<div class="column">--}}
                    {{--<h3>"What a Company"</h3>--}}
                    {{--<p>That is what they all say about us</p>--}}
                {{--</div>--}}
                {{--<div class="column">--}}
                    {{--<h3>"I shouldn't have gone with their competitor."</h3>--}}
                    {{--<p>--}}
                        {{--<img src="https://semantic-ui.com/examples/assets/images/avatar/nan.jpg"--}}
                             {{--class="ui avatar image"> <b>Nan</b> Chief Fun Officer Acme Toys--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="ui vertical stripe segment">--}}
        {{--<div class="ui text container">--}}
            {{--<h3 class="ui header">Breaking The Grid, Grabs Your Attention</h3>--}}
            {{--<p>Instead of focusing on content creation and hard work, we have learned how to master the art of doing--}}
                {{--nothing by providing massive amounts of whitespace and generic content that can seem massive, monolithic--}}
                {{--and worth your attention.</p>--}}
            {{--<a class="ui large button">Read More</a>--}}
            {{--<h4 class="ui horizontal header divider">--}}
                {{--<a href="#">Case Studies</a>--}}
            {{--</h4>--}}
            {{--<h3 class="ui header">Did We Tell You About Our Bananas?</h3>--}}
            {{--<p>Yes I know you probably disregarded the earlier boasts as non-sequitur filler content, but its really--}}
                {{--true. It took years of gene splicing and combinatory DNA research, but our bananas can really dance.</p>--}}
            {{--<a class="ui large button">I'm Still Quite Interested</a>--}}
        {{--</div>--}}
    {{--</div>--}}

@stop