<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 25.05.2017
 * Time: 19:53:10
 */

HTML::macro('hello', function ($parameters){
    if (!is_array($parameters)){
        $parameters = array($parameters);
    }
    $html = '<ul>';
    foreach ($parameters as $key => $value)
        $html .= '<li>'.$value.'</li>';
    $html .= '</ul>';
    return $html;
});