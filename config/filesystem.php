<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 28.05.2017
 * Time: 14:04:12
 */
return [
    'default' => 'local',

    'disks' => [

        'local' => [
            'driver' => \Mugen\FileSystem\LocalFileStorage::class,
            'root' => storage_path('app'),
        ],

        'image' => [
            'driver' => \Mugen\FileSystem\ImageFileStorage::class,
            'root' => storage_path('app/public'),
        ],

        'public' => [
            'driver' => \Mugen\FileSystem\LocalFileStorage::class,
            'root' => storage_path('app/public'),
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => 'your-key',
            'secret' => 'your-secret',
            'region' => 'your-region',
            'bucket' => 'your-bucket',
        ],

    ],
];