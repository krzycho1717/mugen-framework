<?php

/*
 * Main Application config file
 */

return [
    'default_locale' => 'pl',
    'fallback_locale' => 'en',
    'debug' => env('APP_DEBUG', true),
    'services' => [
        'request' => \Mugen\Request::class
    ],
    'aliases' => [
        'Config'    => \Mugen\Config::class,
        'Database'  => \Mugen\Database::class,
        'HTML'      => \Mugen\HTML::class,
        'Lang'      => \Mugen\Lang::class,
        'Route'     => \Mugen\Route::class,
        'View'      => \Mugen\View::class,
        'URL'       => \Mugen\URL::class,
        'Session'   => \Mugen\Session::class
    ]
];