/**
 * Created by Krzysztof Stec on 23.05.2017.
 */

var
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserify = require('gulp-browserify');

gulp.task('scripts', function() {
    // Single entry point to browserify
    gulp.src('./resources/assets/js/*.js')
        .pipe(browserify())
        .pipe(gulp.dest('./public/js'))
});

gulp.task('sass', function () {
    return gulp.src('./resources/assets/sass/*.scss')
        .pipe(sass()
            .on('error', sass.logError)
        )
        .pipe(gulp.dest('./public/css'));
})