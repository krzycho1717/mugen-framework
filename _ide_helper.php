<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 25.05.2017
 * Time: 20:34:48
 */

namespace{
    /**
     * Mugen\HTML
     *
     * @method static void macro($name, Closure $body_callback);
     */
    class HTML {}
    /**
     * Mugen\Lang
     *
     * @method static string|array get($path)
     * @method static string getCurrent()
     * @method static void setLocale($l)
     * @method static void detectLocale($path)
     */
    class Lang {}
}