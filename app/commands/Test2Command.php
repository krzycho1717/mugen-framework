<?php

namespace App\Commands;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 26.05.2017
 * Time: 15:46:33
 */

use Mugen\Command;

class Test2Command extends Command
{
    protected $signature = 'test2-command';
    protected $description = 'Simple test2 command';

    protected function configure()
    {
        $this->addArgument('auto-send', \Mugen\InputArgument::REQUIRED, 'Auto-send?');
    }
}