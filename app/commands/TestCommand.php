<?php

namespace App\Commands;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 26.05.2017
 * Time: 15:46:33
 */

use Mugen\Command;

class TestCommand extends Command
{
    protected $signature = 'make:console {argument} {--command=}';
    protected $description = 'Simple test command';

    protected function configure()
    {
        $this->addArgument('argument', \Mugen\InputArgument::REQUIRED, 'Auto-send?');
        $this->addOption('command', \Mugen\InputOption::VALUE_REQUIRED, 'Auto-send?');
    }
}