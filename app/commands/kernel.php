<?php
require __DIR__ . '/../../lib/helpers.php';
require __DIR__ . '/../../vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 26.05.2017
 * Time: 15:46:16
 */

class Kernel
{
    protected static $commandsClasses = [
        \App\Commands\TestCommand::class,
        \App\Commands\Test2Command::class
    ];

    public static function init($args=null)
    {
        $app = new \Mugen\Application();
        $test = new \App\Migration\UserTable();
        $test->up();
        //foreach (static::$commandsClasses as $command)
        //{
        //    $commandClass = new ReflectionClass($command);
        //    $inst = $commandClass->newInstance();
        //    # print $inst->getSignature() . PHP_EOL;
        //    $match = $inst->parseSignature();
        //    if ($args[1] === $match[0]) {
        //        #print $match[0] . PHP_EOL;
        //        array_splice($args, 0, 1);
        //        $inst->execute($args);
        //    }
        //}
    }
}
$re = '/([^\s]+?)(?:\s|(?<!\\\\\\\\)"|(?<!\\\\\\\\)\'|$)/';
$str = implode(" ", $argv);
preg_match_all($re, $str, $matches);

//print 'Input: ' . PHP_EOL;
//print_r($matches[0]);
//print PHP_EOL;

Kernel::init($matches[0]);