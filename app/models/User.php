<?php

namespace App\Models;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 04.11.2016
 * Time: 17:21:07
 */
class User extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = ['name', 'surname', 'email', 'password'];
}