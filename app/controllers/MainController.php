<?php
namespace App\Controllers;

use App\Models\User;
use Mugen\Log;
use Mugen\Storage;
use Mugen\UploadError;
use Mugen\View;
use Mugen\Request;

class MainController extends \Mugen\Controller
{
    public function index()
    {
        return View::make('test');
    }

    public function file()
    {
        $users = User::all();
        return \Mugen\View::make('file')->with('users', $users);
    }

    public function fileUpload(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file('file') as $file) {
                if ($file->isValid()) {
                    Log::json([
                        'valid' => true,
                        'file' => $file->name
                    ]);
                    Storage::disk('image')->put($file->name, $file);
                }else
                    Log::json([
                        'valid' => false,
                        'file' => $file,
                        'error' => UploadError::values[$file->error]
                    ]);
                    //Log::json('something is wrong: '.$file->error.';'.$file->name);
            }
        }
    }

    public function testCreate()
    {
        \Mugen\Cookie::set('auth', true);
    }

    public function testRemove()
    {
        \Mugen\Cookie::delete('auth');
    }

}