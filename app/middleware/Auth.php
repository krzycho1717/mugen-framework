<?php

/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 07.11.2016
 * Time: 18:12:03
 */

namespace App\Middleware;

use Mugen\Error;
use Mugen\Middleware;
use Mugen\Cookie;
use Mugen\Request;
use Closure;

class Auth extends Middleware
{
    public static function handle(Request $request, Closure $next)
    {
        if (Cookie::get('auth') != null && Cookie::get('auth'))
            return $next($request);
        else{
            Error::E403();
        }
    }
}