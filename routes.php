<?php
use Mugen\Route;

Route::get('/', 'MainController@index');
Route::get('/file', 'MainController@file');
Route::post('/file', 'MainController@fileUpload');

Route::get('/create', 'MainController@testCreate');
Route::get('/remove', 'MainController@testRemove');

Route::group(['middleware' => 'Auth', 'prefix' => 'admin'], function (){
    Route::get('/{test}', function (\Mugen\Application $app, \Mugen\FileSystem\LocalFileStorage $fileStorage, $test){
        \Mugen\Log::d($test);
        \Mugen\Log::d($fileStorage->getName());
    });
});

Route::any('/{test}', function (\Mugen\Application $app, \Mugen\FileSystem\LocalFileStorage $fileStorage, $test){
    \Mugen\Log::d($test);
    \Mugen\Log::d($fileStorage->getName());
});

Route::get('test1/{t}', function (\Mugen\Request $request, \Mugen\Application $app, $t){
    \Mugen\Log::d($request->path());
    \Mugen\Log::d($app != null ? 'Application':null);
    \Mugen\Log::d($t);
});