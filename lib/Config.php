<?php

namespace Mugen;
use Illuminate\Filesystem\FileNotFoundException;

/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 04.11.2016
 * Time: 14:31:34
 */
class Config
{
    const config_dir = __DIR__.'/../config';

    public static function get($path){
        $_PATH = self::parsePath($path);
        $data = self::load($_PATH[0]);
        if(!is_array($data)){
            throw new \Exception('The config file \''.$_PATH[0].'\' doesn\'t contain an array!');
            return null;
        }
        $value = $data;
        for ($i = 1; $i < count($_PATH); $i++) {
            if (array_key_exists($_PATH[$i], $value)) {
                $value = $value[$_PATH[$i]];
            }else{
                return null;
            }
        }
        return $value;
    }

    private static function load($name){
        if (!file_exists(self::config_dir.'/'.$name.'.php'))
            throw new \Exception('Config file \''.self::config_dir.'/'.$name.'.php\' not found');
        return include self::config_dir.'/'.$name.'.php';
    }

    private static function parsePath($path){
        $pa = explode('.', $path);
        if(count($pa) >= 2){
            if(!file_exists(self::config_dir)) return null;
            return $pa;
        }
        return null;
    }

    private static function parse($data, &$result, $prefix = "") {

        if (is_array($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                self::parse($value, $result, "{$prefix}.{$key}");
            }
        } else {
            $result[substr($prefix, 1)] = $data;
        }
    }

    private static function update(&$a, $p, $v){
        if(empty($p)) {$a = $v; return true;}
        if(array_key_exists($p[0], $a)){
            $pt = $p[0];
            array_splice($p, 0, 1);
            return self::update($a[$pt], $p, $v);
        }
        return false;
    }
}