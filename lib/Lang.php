<?php
namespace Mugen;

class Lang
{
    const locale_dir = '../resources/locale';
    const locale_coockie_key = 'locale';

    private static $_locale;

    // example: 'ui.start.header'
    public static function get($path){
        $pa = explode('.', $path);
        if(count($pa) >= 2){
            if(!file_exists(Lang::locale_dir.'/'.Lang::$_locale)) return $path;
            $la = include Lang::locale_dir.'/'.Lang::$_locale.'/'.$pa[0].'.php';
            if(!is_array($la)){
                return $path;
            }
            $value = $la;
            for ($i = 1; $i < count($pa); $i++) {
                if (array_key_exists($pa[$i], $value)) {
                    $value = $value[$pa[$i]];
                }else{
                    return $path;
                }
            }
            return $value;
        }
        return $path;
    }

    public static function getCurrent()
    {
        return Lang::$_locale;
    }

    public static function setLocale($l){
        // one year duration
        setcookie(Lang::locale_coockie_key, $l, time()+31556926);
    }

    public static function detectLocale(){
        if(!isset($_COOKIE['locale'])) {
            Lang::$_locale = Config::get('app.default_locale');
        }else{
            Lang::$_locale = $_COOKIE['locale'];
        }
    }
}