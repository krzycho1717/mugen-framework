<?php
namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 04.11.2016
 * Time: 13:46:23
 */
class Log
{
    public static function d($var){
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

    public static function json($var)
    {
        echo json_encode($var);
    }
}