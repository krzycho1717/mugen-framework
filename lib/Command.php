<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 26.05.2017
 * Time: 14:21:24
 */
# (?<command>[a-zA-Z]+)(?<option>\:[\w]+)?[ ](?<arguments>[a-zA-Z0-9]+)?
# category:command {arguments}              command with argument
# category:command {arguments?}             command with optional argument
# category:command {--option=value}         command with option with default value
# category:command {--option}         command with option with boolean value
# category:command {--option=}              command with option with value

class Command
{
    protected $arguments = array();
    protected $options = array();
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        $this->configure();
    }

    public function getSignature()
    {
        return $this->signature;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    }

    public function execute($args=array())
    {
        $i = 0;
        foreach ($this->parseSignature() as $sig){

        }
    }

    public function arguments()
    {
        return $this->arguments;
    }

    public function addArgument($name, $required, $text){
        if (!array_has($this->arguments, $name)){
            $this->arguments[$name] = array(
                'required'  => $required,
                'text'      => $text
            );
        }
        return $this;
    }

    public function getArgument($name){
        if (array_has($this->arguments, $name)){
            return $this->arguments[$name];
        }
        return null;
    }

    public function options()
    {
        return $this->options;
    }

    public function addOption($name, $required, $text){
        if (!array_has($this->options, $name)){
            $this->options[$name] = array(
                'required'  => $required,
                'text'      => $text
            );
        }
        return $this;
    }

    public function getOption($name){
        if (array_has($this->options, $name)){
            return $this->options[$name];
        }
        return null;
    }

    protected function configure()
    {

    }

    public function parseSignature()
    {
        $re = '/([^\s]+?)(?:\s|(?<!\\\\\\\\)"|(?<!\\\\\\\\)\'|$)/';
        preg_match_all($re, $this->signature, $matches);
        $match = $matches[0];
        if (strpos($match[0], ':') === false){
            $match[0] = 'app:' . $match[0];
        }
        return $match;
    }
}

abstract class InputArgument
{
    const REQUIRED = 0;
    const OPTIONAL = 1;
}

abstract class InputOption
{
    const VALUE_REQUIRED = 0;
    const VALUE_NONE = 1;
    const VALUE_OPTIONAL = 2;
}