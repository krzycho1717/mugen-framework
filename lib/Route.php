<?php
namespace Mugen;

use Exceptions\MissingControllerException;
use Exceptions\MissingControllerMethodException;

/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 27.10.2016
 * Time: 16:13:40
 */


class Route
{
    private static $in_group = false;
    private static $routes = [];

    public static function group(array $options, $callback){
        self::$in_group = true;
        $default_options = [
            'method'    => 'group',
            'routes'    => [],
            'options'   => array_merge([
                'prefix'     => null,
                'middleware' => null
            ], $options)
        ];
        $default_options['options']['prefix'] = ($default_options['options']['prefix'] != null && strlen($default_options['options']['prefix']) >= 2)? ltrim($default_options['options']['prefix'], '/') : $default_options['options']['prefix'];
        array_push(Route::$routes, $default_options);

        #Log::d($default_options);

        $callback();
        self::$in_group = false;
    }

    private static function addMethod($method, $pattern, $controller){
        if (self::$in_group){
            static::addMethodWithOptions($method, $pattern, Route::$routes[count(Route::$routes)-1]['options'], $controller);
            return;
        }
        static::addMethodWithOptions($method, $pattern, array(), $controller);
    }

    private static function addMethodWithOptions($method, $pattern, array $options, $controller)
    {
        $pattern = (strlen($pattern) >= 2)? ltrim($pattern, '/') : $pattern;
        $url_pattern = Route::pregReplaceAll('#({[0-9a-zA-Z]+})#', '([a-zA-Z0-9]+)', $pattern);
        if (array_has($options, 'prefix') && $options['prefix'] != null){
            $url_pattern = $options['prefix'] . '/' . ($url_pattern == '/' ? '' : $url_pattern);
        }else{
            if ($url_pattern === '([a-zA-Z0-9]+)')
                $url_pattern = '/'.$url_pattern;
        }
        if(self::$in_group){
            Route::$routes[count(Route::$routes)-1]['routes'][] = [
                'method'      => $method,
                'url_pattern' => '@^' . $url_pattern . '[\w\?\=\&]*$@',
                'controller'  => $controller
            ];
            return;
        }
        Route::$routes[] = [
            'method'      => $method,
            'url_pattern' => '@^'.$url_pattern.'[\w\?\=\&]*$@',
            'controller'  => $controller,
            'options'   => array_merge([
                'prefix'     => null,
                'middleware' => null
            ], $options)
        ];
    }

    public static function run(Application $app){
        //$url = (strlen($_SERVER['REQUEST_URI']) > 1)? ltrim($_SERVER['REQUEST_URI'], '/') : $_SERVER['REQUEST_URI'];
        //$method = $_SERVER['REQUEST_METHOD'];
        $request = new Request();
        $url = $request->path();
        $method = $request->method();

        //Log::d(self::$routes);

        foreach (self::$routes as $route){
            if($route['method'] == 'group'){
                self::buildGroup($route, $method, $url, $app);
            }else {
                if (self::build($route, $method, $url, $app, $route['options'])){
                    exit();
                }
            }
        }
        Error::E404();
    }

    private static function buildGroup($group, $method, $url, $app)
    {
        foreach ($group['routes'] as $route_g){
            if (self::build($route_g, $method, $url, $app, $group['options'])){
                exit();
            }
        }
    }

    private static function build($route, $method, $url, $app, $options){
        if ($route['method'] == 'any' || $route['method'] == strtolower($method)) {
            if (preg_match($route['url_pattern'], $url, $match_result)) {
                if (is_callable($route['controller'])) {
                    return self::dispatch_func($app, $options['middleware'], $route['controller'], $match_result);
                } else {
                    $controller = explode('@', $route['controller']);
                    $path = '../app/controllers/' . $controller[0] . '.php';
                    if (file_exists($path)) {
                        #require_once $path;
                        $refl = new \ReflectionClass('\\App\\Controllers\\'.$controller[0]);
                        #$c = new $controller[0]($app);
                        $c = $refl->newInstance($app);
                        // more than one parameter in URL
                        if (method_exists($c, $controller[1])) {
                            return self::dispatch($app, $options['middleware'], $c, $controller, $match_result);
                        } else {
                            throw new MissingControllerMethodException('Controller method ' . $controller[1] . ' was not found');
                        }
                    } else {
                        //echo $path.' not exists';
                        throw new MissingControllerException('Controller ' . $controller[0] . ' was not found');
                    }
                }
            }
        }
        return false;
    }

    private static function pregReplaceAll($find, $replacement, $s) {
        while(preg_match($find, $s)) {
            $s = preg_replace($find, $replacement, $s);
        }
        return $s;
    }

    /**
     * @param Application $app
     * @param $middleware
     * @param $controller_instance
     * @param $controller
     * @param $match_result
     * @return bool
     */
    private static function dispatch(Application $app, $middleware, $controller_instance, $controller, $match_result)
    {
        if ($middleware != null)
            $middleware = '\\App\\Middleware\\' . $middleware . '::handle';
        else
            $middleware = '\\Mugen\\DefaultMiddleware' . $middleware . '::handle';

        call_user_func($middleware, new Request(), function () use ($controller_instance, $controller, $match_result, $app) {
            $controllerMethod = new \ReflectionMethod('\\App\\Controllers\\'.$controller[0],$controller[1]);
            $params = $controllerMethod->getParameters();
            $resolved = self::resolveDependencies($params, $app, $match_result);
            echo call_user_func_array(array($controller_instance, $controller[1]), $resolved);
        });

        return true;
    }

    /**
     * @param Application $app
     * @param $middleware
     * @param $func
     * @param $match_result
     * @return bool
     */
    private static function dispatch_func(Application $app, $middleware, $func, $match_result)
    {
        if ($middleware != null)
            $middleware = '\\App\\Middleware\\' . $middleware . '::handle';
        else
            $middleware = '\\Mugen\\DefaultMiddleware' . $middleware . '::handle';

        call_user_func($middleware, new Request(), function () use ($func, $match_result, $app) {
            $controllerMethod = new \ReflectionFunction($func);
            $params = $controllerMethod->getParameters();
            $resolved = self::resolveDependencies($params, $app, $match_result);
            echo call_user_func_array($func, $resolved);
        });
        return true;
    }

    private static function resolveDependencies($params, Application $app, $args)
    {
        $args =  array_slice($args, 1, count($args) - 1);
        $depdn = [];
        for ($i = 0; $i < count($params); $i++){
            if (is_null($params[$i]->getClass())){
                continue;
            }
            if (!class_exists($params[$i]->getClass()->getName())) {
                $depdn[] = null;
                continue;
            }
            if ($params[$i]->getClass()->getName() === Application::class) {
                $depdn[] = $app;
                continue;
            }
            try{
                $refl = new \ReflectionClass($params[$i]->getClass()->getName());
                $tmp = $refl->newInstance();
                $depdn[] = $tmp;
            }catch (\Exception $exception){
                $depdn[] = null;
            }

        }

        $result = $depdn;
        foreach ($args as $arg){
            $result[] = $arg;
        }
        return $result;
    }

    public static function __callStatic($name, $arguments)
    {
        if (in_array($name, ['get', 'post', 'any', 'delete', 'put'])){
            $num = count($arguments);
            array_unshift($arguments, $name);
            $name = $num == 2 ? 'addMethod' : 'addMethodWithOptions';
            //Log::d($arguments);
            call_user_func_array('self::'.$name, $arguments);
            return;
        }
        if (!method_exists('self', $name)){
            throw new \BadMethodCallException();
        }
        call_user_func_array('self::'.$name, $arguments);
    }


}