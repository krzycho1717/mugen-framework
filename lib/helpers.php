<?php

if (!function_exists('csrf')) {
    function csrf()
    {
        return \Mugen\Session::get('token');
    }
}

if (!function_exists('asset')) {
    function asset($path)
    {
        return env('APP_URL') . '/assets' . $path;
    }
}

if (!function_exists('url')) {
    function url($uri)
    {
        return env('APP_URL') . '/' . $uri;
    }
}

if (!function_exists('base_path')) {
    function base_path($path)
    {
        $base = str_contains($_SERVER['DOCUMENT_ROOT'], '/public') ? str_replace('/public', '', $_SERVER['DOCUMENT_ROOT']) : $_SERVER['DOCUMENT_ROOT'];
        return $base . (substr($path, 0, 1) === '/' ? $path : '/' . $path);
    }
}

if (!function_exists('resource_path')) {
    function resource_path($path){
        return base_path('resources/' . $path);
    }
}

if (!function_exists('storage_path')) {
    function storage_path($path){
        return base_path('storage/' . $path);
    }
}

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (strlen($value) > 1 && startsWith($value, '"') && endsWith($value, '"')) {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (! function_exists('contains')) {
    /**
     * Determine if a given string contains a given substring.
     *
     * @param  string $haystack
     * @param  string|array $needles
     * @return bool
     */
    function contains($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle != '' && mb_strpos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }
}

if (! function_exists('is')) {
    /**
     * Determine if a given string matches a given pattern.
     *
     * @param  string  $pattern
     * @param  string  $value
     * @return bool
     */
    function is($pattern, $value)
    {
        if ($pattern == $value) {
            return true;
        }

        $pattern = preg_quote($pattern, '#');

        // Asterisks are translated into zero-or-more regular expression wildcards
        // to make it convenient to check if the strings starts with the given
        // pattern such as "library/*", making any string check convenient.
        $pattern = str_replace('\*', '.*', $pattern);

        return (bool) preg_match('#^'.$pattern.'\z#u', $value);
    }
}

if (! function_exists('startsWith')) {
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string  $haystack
     * @param  string|array  $needles
     * @return bool
     */
    function startsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle != '' && substr($haystack, 0, strlen($needle)) === (string) $needle) {
                return true;
            }
        }

        return false;
    }
}

if (! function_exists('endsWith')) {
    /**
     * Determine if a given string ends with a given substring.
     *
     * @param  string $haystack
     * @param  string|array $needles
     * @return bool
     */
    function endsWith($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if (substr($haystack, -strlen($needle)) === (string)$needle) {
                return true;
            }
        }

        return false;
    }
}

if (! function_exists('isMobile')){
    function isMobile()
    {
        $uaFull = strtolower($_SERVER['HTTP_USER_AGENT']);
        $uaStart = substr($uaFull, 0, 4);

        $uaPhone = [
            '(android|bb\d+|meego).+mobile',
            'avantgo',
            'bada\/',
            'blackberry',
            'blazer',
            'compal',
            'elaine',
            'fennec',
            'hiptop',
            'iemobile',
            'ip(hone|od)',
            'iris',
            'kindle',
            'lge ',
            'maemo',
            'midp',
            'mmp',
            'mobile.+firefox',
            'netfront',
            'opera m(ob|in)i',
            'palm( os)?',
            'phone',
            'p(ixi|re)\/',
            'plucker',
            'pocket',
            'psp',
            'series(4|6)0',
            'symbian',
            'treo',
            'up\.(browser|link)',
            'vodafone',
            'wap',
            'windows ce',
            'xda',
            'xiino'
        ];

        $uaMobile = [
            '1207',
            '6310',
            '6590',
            '3gso',
            '4thp',
            '50[1-6]i',
            '770s',
            '802s',
            'a wa',
            'abac|ac(er|oo|s\-)',
            'ai(ko|rn)',
            'al(av|ca|co)',
            'amoi',
            'an(ex|ny|yw)',
            'aptu',
            'ar(ch|go)',
            'as(te|us)',
            'attw',
            'au(di|\-m|r |s )',
            'avan',
            'be(ck|ll|nq)',
            'bi(lb|rd)',
            'bl(ac|az)',
            'br(e|v)w',
            'bumb',
            'bw\-(n|u)',
            'c55\/',
            'capi',
            'ccwa',
            'cdm\-',
            'cell',
            'chtm',
            'cldc',
            'cmd\-',
            'co(mp|nd)',
            'craw',
            'da(it|ll|ng)',
            'dbte',
            'dc\-s',
            'devi',
            'dica',
            'dmob',
            'do(c|p)o',
            'ds(12|\-d)',
            'el(49|ai)',
            'em(l2|ul)',
            'er(ic|k0)',
            'esl8',
            'ez([4-7]0|os|wa|ze)',
            'fetc',
            'fly(\-|_)',
            'g1 u',
            'g560',
            'gene',
            'gf\-5',
            'g\-mo',
            'go(\.w|od)',
            'gr(ad|un)',
            'haie',
            'hcit',
            'hd\-(m|p|t)',
            'hei\-',
            'hi(pt|ta)',
            'hp( i|ip)',
            'hs\-c',
            'ht(c(\-| |_|a|g|p|s|t)|tp)',
            'hu(aw|tc)',
            'i\-(20|go|ma)',
            'i230',
            'iac( |\-|\/)',
            'ibro',
            'idea',
            'ig01',
            'ikom',
            'im1k',
            'inno',
            'ipaq',
            'iris',
            'ja(t|v)a',
            'jbro',
            'jemu',
            'jigs',
            'kddi',
            'keji',
            'kgt( |\/)',
            'klon',
            'kpt ',
            'kwc\-',
            'kyo(c|k)',
            'le(no|xi)',
            'lg( g|\/(k|l|u)|50|54|\-[a-w])',
            'libw',
            'lynx',
            'm1\-w',
            'm3ga',
            'm50\/',
            'ma(te|ui|xo)',
            'mc(01|21|ca)',
            'm\-cr',
            'me(rc|ri)',
            'mi(o8|oa|ts)',
            'mmef',
            'mo(01|02|bi|de|do|t(\-| |o|v)|zz)',
            'mt(50|p1|v )',
            'mwbp',
            'mywa',
            'n10[0-2]',
            'n20[2-3]',
            'n30(0|2)',
            'n50(0|2|5)',
            'n7(0(0|1)|10)',
            'ne((c|m)\-|on|tf|wf|wg|wt)',
            'nok(6|i)',
            'nzph',
            'o2im',
            'op(ti|wv)',
            'oran',
            'owg1',
            'p800',
            'pan(a|d|t)',
            'pdxg',
            'pg(13|\-([1-8]|c))',
            'phil',
            'pire',
            'pl(ay|uc)',
            'pn\-2',
            'po(ck|rt|se)',
            'prox',
            'psio',
            'pt\-g',
            'qa\-a',
            'qc(07|12|21|32|60|\-[2-7]|i\-)',
            'qtek',
            'r380',
            'r600',
            'raks',
            'rim9',
            'ro(ve|zo)',
            's55\/',
            'sa(ge|ma|mm|ms|ny|va)',
            'sc(01|h\-|oo|p\-)',
            'sdk\/',
            'se(c(\-|0|1)|47|mc|nd|ri)',
            'sgh\-',
            'shar',
            'sie(\-|m)',
            'sk\-0',
            'sl(45|id)',
            'sm(al|ar|b3|it|t5)',
            'so(ft|ny)',
            'sp(01|h\-|v\-|v )',
            'sy(01|mb)',
            't2(18|50)',
            't6(00|10|18)',
            'ta(gt|lk)',
            'tcl\-',
            'tdg\-',
            'tel(i|m)',
            'tim\-',
            't\-mo',
            'to(pl|sh)',
            'ts(70|m\-|m3|m5)',
            'tx\-9',
            'up(\.b|g1|si)',
            'utst',
            'v400',
            'v750',
            'veri',
            'vi(rg|te)',
            'vk(40|5[0-3]|\-v)',
            'vm40',
            'voda',
            'vulc',
            'vx(52|53|60|61|70|80|81|83|85|98)',
            'w3c(\-| )',
            'webc',
            'whit',
            'wi(g |nc|nw)',
            'wmlb',
            'wonu',
            'x700',
            'yas\-',
            'your',
            'zeto',
            'zte\-'
        ];

        $isPhone = preg_match('/' . implode($uaPhone, '|') . '/i', $uaFull);
        $isMobile = preg_match('/' . implode($uaMobile, '|') . '/i', $uaStart);

        return $isPhone || $isMobile;
    }
}