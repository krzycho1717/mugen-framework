<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 04.11.2016
 * Time: 13:24:23
 */
abstract class Controller
{
    protected $application;

    /**
     * Controller constructor.
     * @param Application $application Instance of application
     */
    public final function __construct(Application $application) {
        $this->application = $application;
        $this->initialize();
    }

    protected function initialize(){}

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }
}