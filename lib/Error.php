<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 27.10.2016
 * Time: 17:43:07
 */
class Error
{
    public static function E404(){
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        exit();
    }

    public static function E403(){
        header($_SERVER["SERVER_PROTOCOL"]." 403 Forbidden", true, 403);
        exit();
    }
}