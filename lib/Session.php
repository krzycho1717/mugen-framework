<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 25.05.2017
 * Time: 23:06:05
 */
/**
 * Class Session
 * @package Mugen
 */
class Session
{

    /**
     *
     */
    public static function init()
    {
        session_start();
        if (empty($_SESSION['token'])) {
            $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
        }
    }

    /**
     * Sets or updates value in session
     * @param string $key
     * @param $value
     */
    public static function set($key, $value)
    {
        if (session_status() == PHP_SESSION_NONE) static::init();
        $_SESSION[$key] = $value;
    }

    /**
     * Return value from session
     * @param string $key
     * @return object
     */
    public static function get($key)
    {
        if (session_status() == PHP_SESSION_NONE) static::init();
        if (static::has($key))
            return $_SESSION[$key];
        return null;
    }

    /**
     * Checks if key exists in session
     * @param string $key
     * @return bool
     */
    public static function has($key)
    {
        if (session_status() == PHP_SESSION_NONE) static::init();
        return array_has($_SESSION, $key);
    }

    /**
     * Removes value from session
     * @param string $key
     */
    public static function remove($key)
    {
        if (session_status() != PHP_SESSION_NONE)
            unset($_SESSION[$key]);
    }

    /**
     * Destroys session
     */
    public static function destroy()
    {
        if (session_status() != PHP_SESSION_NONE)
            session_destroy();
    }
}