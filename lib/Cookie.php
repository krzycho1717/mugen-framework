<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 23.05.2017
 * Time: 16:16:08
 */

class Cookie
{
    const hour = 60*60;
    const day = 60*60*24;
    public static function set($key, $value, $exp_time=null)
    {
        if ($exp_time == null){
            $exp_time = time() + Cookie::day;
        }
        setcookie($key, $value, time() + $exp_time);
    }

    public static function get($key)
    {
        if (array_has($_COOKIE, $key)){
            return $_COOKIE[$key];
        }
        return null;
    }

    public static function delete($key)
    {
        if (array_has($_COOKIE, $key)){
            setcookie($key, "", time() - 3600);
        }
    }
}