<?php

/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 07.11.2016
 * Time: 18:10:49
 */

namespace Mugen;

use Closure;

class Middleware
{
    protected static function handle(Request $request, Closure $next){
        return $next($request);
    }
}