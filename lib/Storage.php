<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 28.05.2017
 * Time: 14:38:45
 */
use Mugen\FileSystem\FileStorageInterface;

class Storage
{

    /**
     * @param string $name Name of storage driver
     * @return FileStorageInterface
     */
    public static function disk($name)
    {
        $conf = Config::get('filesystem.disks.'.$name);
        if ($conf != null && class_exists($conf['driver'])){
            $refl = new \ReflectionClass($conf['driver']);
            return $refl->newInstance($name, $conf);
        }
        return null;
    }
}