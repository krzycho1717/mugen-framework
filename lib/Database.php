<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 07.11.2016
 * Time: 15:18:04
 */

use Illuminate\Database\Capsule\Manager as Capsule;

class Database
{
    public static function init(){
        $capsule = new Capsule;
        $capsule->addConnection(array(
            'driver'    => env('DB_CONNECTION', 'mysql'),
            'host'      => env('DB_HOST', 'localhost'),
            'database'  => env('DB_DATABASE', 'gate'),
            'username'  => env('DB_USERNAME', 'root'),
            'password'  => env('DB_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => ''
        ));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}