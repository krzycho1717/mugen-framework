<?php
namespace Mugen;

class URL
{
    public static function link($path){
        return env('APP_URL').ltrim($path, '/');
    }

    public static function vendor($path)
    {
        return env('APP_URL').'/vendor/'.ltrim($path, '/');
    }

    public static function asset($path){
        return env('APP_URL').ltrim($path, '/');
    }

    public static function style($path){
        return env('APP_URL').'/css/'.ltrim($path, '/');
    }

    public static function script($path){
        return env('APP_URL').'/js/'.ltrim($path, '/');
    }

    public static function image($path){
        return env('APP_URL').'/img/'.ltrim($path, '/');
    }

    public static function action($route, $params = null)
    {
        if (str_contains($route, '@')) {
            $controller = explode('@', $route);

        }
    }
}