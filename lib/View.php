<?php
namespace Mugen;

class View
{
    private static $renderer = null;

    public static function init($r)
    {
        static::$renderer = $r;
    }

    public static function make($view, $data = [], $mergeData = []){
        # return View::$renderer->render($name, $param);
        return static::$renderer->view()->make($view, $data, $mergeData);
    }

    private function __construct(){}
}