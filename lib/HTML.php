<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 25.05.2017
 * Time: 18:55:28
 */

use Closure;

class HTML
{
    /**
     * Holds macro list
     * @var array
     */
    private static $macros = array();


    /**
     * Register macro in application
     * @param $name string Name of macro
     * @param Closure $body_callback Function returning html body
     * @throws \Exception
     */
    public static function macro($name, Closure $body_callback)
    {
        if (array_has(static::$macros, $name)){
            throw new \Exception('Macro with name \''.$name.'\' already exists');
        }
        static::$macros[$name] = $body_callback;
    }
    /**
     * Dynamically handle calls to the macros.
     *
     * @param  string $method
     * @param  array  $parameters
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function __callStatic($method, $parameters)
    {
        static::$macros = array();
        foreach (glob(base_path('resources/macros/*.macro.php')) as $fileName){
            if (file_exists($fileName)){
                require_once $fileName;
            }
        }
        if (array_has(static::$macros, $method)){
            return call_user_func_array(static::$macros[$method], $parameters);
        }else{
            throw new \Exception('Coudn\'t find macro with name \''.$method.'\'');
        }
    }
}