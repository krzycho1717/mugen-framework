<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 29.05.2017
 * Time: 18:30:21
 */
class UploadedFile
{
    private $name;
    private $type;
    private $error = UploadError::UPLOAD_ERR_NO_FILE;
    private $tmp_name;
    private $size;


    /**
     * InputFile constructor.
     * @param $name
     * @param $type
     * @param $tmp_name
     * @param $error UploadError|int
     * @param $size
     */
    public function __construct($name, $type, $tmp_name, $error, $size)
    {
        $this->name = $name;
        $this->type = $type;
        $this->tmp_name = $tmp_name;
        $this->error = $error;
        $this->size = $size;
    }

    public function isValid()
    {
        return $this->error == UPLOAD_ERR_OK;
    }

    function __get($name)
    {
        if (!property_exists(UploadedFile::class, $name)) {
            throw new \Exception ('Property '.$name.' is not defined');
        }
        return $this->$name;
    }

    function __set($name, $value)
    {
        throw new Exception ('Can\'t set property $name');
    }


}

class UploadError{
    const UPLOAD_ERR_OK = 0;
    const UPLOAD_ERR_INI_SIZE = 1;
    const UPLOAD_ERR_FORM_SIZE = 2;
    const UPLOAD_ERR_PARTIAL = 3;
    const UPLOAD_ERR_NO_FILE = 4;
    const UPLOAD_ERR_NO_TMP_DIR = 5;
    const UPLOAD_ERR_CANT_WRITE = 6;
    const UPLOAD_ERR_EXTENSION = 7;
    const values = array(
        0 => 'UPLOAD_ERR_OK',
        1 => 'UPLOAD_ERR_INI_SIZE',
        2 => 'UPLOAD_ERR_FORM_SIZE',
        3 => 'UPLOAD_ERR_PARTIAL',
        4 => 'UPLOAD_ERR_NO_FILE',
        5 => 'UPLOAD_ERR_NO_TMP_DIR',
        6 => 'UPLOAD_ERR_CANT_WRITE',
        7 => 'UPLOAD_ERR_EXTENSION',
    );
}