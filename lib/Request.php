<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 07.11.2016
 * Time: 17:47:09
 */

namespace Mugen;


class Request
{
    /**
     * @return mixed
     */
    public function method()
    {
        if ($this->has('_method')){
            return $this->input('_method');
        }
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @param $method
     * @return bool
     */
    public function isMethod($method)
    {
        return $this->method() === $method;
    }

    /**
     * @return string
     */
    public function path(){
        return (strlen($_SERVER['REQUEST_URI']) > 1)? ltrim($_SERVER['REQUEST_URI'], '/') : $_SERVER['REQUEST_URI'];
    }

    /**
     * @return string
     */
    public function url()
    {
        return strtok($_SERVER["REQUEST_URI"],'?');
    }

    /**
     * @return string
     */
    public function fullUrl()
    {
        return strtok($_SERVER["REQUEST_URI"]);
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $_POST);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function input($name)
    {
        if ($this->has($name))
            return $_POST[$name];
    }

    /**
     *
     */
    public function cookie()
    {
        
    }


    /**
     * Gets file by name from $_FILE
     * @param $name
     * @return UploadedFile;
     */
    public function file($name)
    {
        if (count($_FILES) == 0) return null;
        if (! $this->hasFile($name)) return null;
        $f = array();
        for ($i = 0; $i < count($_FILES[$name]['name']); $i++){
            $file = new UploadedFile($_FILES[$name]['name'][$i], $_FILES[$name]['type'][$i], $_FILES[$name]['tmp_name'][$i], $_FILES[$name]['error'][$i], $_FILES[$name]['size'][$i]);
            $f[$i] = $file;
        }
        return $f;
    }

    /**
     * Checks if has file
     * @param $name
     * @return bool
     */
    public function hasFile($name)
    {
        return array_key_exists($name, $_FILES);
    }

    public function isAjax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    public function ip()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}