<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 07.11.2016
 * Time: 13:49:36
 */

namespace Mugen\Exceptions;
use Exception;

class MissingControllerMethodException extends Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}