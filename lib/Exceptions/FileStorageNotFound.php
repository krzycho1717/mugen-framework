<?php

namespace Mugen\Exceptions;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 29.05.2017
 * Time: 20:11:37
 */

class FileStorageNotFound extends \Exception
{
}