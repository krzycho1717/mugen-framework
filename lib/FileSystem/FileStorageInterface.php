<?php

namespace Mugen\FileSystem;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 28.05.2017
 * Time: 13:57:27
 */

abstract class FileStorageInterface
{
    protected $name;
    protected $config = null;

    public function __construct($name, array $config)
    {
        $this->name = $name;
        $this->config = $config;
    }

    public function getName()
    {
        return $this->name;
    }
    public function put($filename, $file){}
    public function get($filename){}
    public function exists($filename){ return file_exists($filename); }
    public function url($filename){}
    public function size($filename){}
    public function lastModified($filename){}
    public function copy($old_filename, $new_filename){}
    public function move($old_filename, $new_filename){ move_uploaded_file($old_filename, $new_filename); }
    public function delete($filename){}
    public function files($directory){}
    public function allFiles($directory){}
    public function directories($directory){}
    public function allDirectories($directory){}
}