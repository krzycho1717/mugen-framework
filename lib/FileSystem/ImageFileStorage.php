<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 29.05.2017
 * Time: 20:05:32
 */

namespace Mugen\FileSystem;

use Intervention\Image\ImageManagerStatic as Image;
use Mugen\Log;
use Mugen\UploadedFile;

class ImageFileStorage extends FileStorageInterface
{

    public function put($filename, $file)
    {
        if (is_a($file, UploadedFile::class)){
            //Log::d('saving image');
            $img = Image::make($file->tmp_name);
            $img->fit(300, 200);
            $img->save($this->config['root'].'/'.$filename);
        }else{
            //Log::d('$file is not UploadedFile class');
        }
    }

}