<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 28.05.2017
 * Time: 14:00:54
 */

namespace Mugen\FileSystem;

use Mugen\UploadedFile;

class LocalFileStorage extends FileStorageInterface
{

    public function put($filename, $file)
    {
        if (is_a($file, UploadedFile::class)){
            $this->move($file->tmp_name, $this->config['root'].'/'.$filename);
        }else{
            file_put_contents($this->config['root'].'/'.$filename, $file);
        }
    }

}