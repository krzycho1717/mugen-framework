<?php

namespace Mugen;
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 23.05.2017
 * Time: 16:50:05
 */

use Closure;

class DefaultMiddleware extends Middleware
{
    public static function handle(Request $request, Closure $next)
    {
        return $next($request);
    }
}