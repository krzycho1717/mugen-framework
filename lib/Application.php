<?php

namespace Mugen;

/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 27.05.2017
 * Time: 13:31:45
 */
use Philo\Blade\Blade;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Application
{
    protected $container;
    /**
     * Application constructor.
     */
    public function __construct()
    {
        // loading .ENV file
        $dotenv = new \Dotenv\Dotenv(__DIR__.'/../');
        $dotenv->load();
        // Check if debug is turned on
        $deb = Config::get('app.debug');
        error_reporting($deb ? E_ALL : 0);
        $aliases = Config::get('app.aliases');

        // Loads aliases
        foreach ($aliases as $aliase => $class) {
            class_alias($class, $aliase);
        }

        $this->container = new ContainerBuilder();
        $services = Config::get('app.services');
        foreach ($services as $name => $class){
            $this->container->register($name, $class);
        }

        // Init session
        Session::init();

        // Database
        Database::init();

        // Detect locale
        Lang::detectLocale();

        // Blade Engine
        $paths = array(resource_path('views'));
        View::init(new Blade($paths, base_path('cache')));
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function run()
    {
        // Router
        require_once '../routes.php';
        try {
            Route::run($this);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}