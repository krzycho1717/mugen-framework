#!/usr/bin/python

import sys
import os

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# print bcolors.OKGREEN, 'test w', bcolors.ENDC
# print bcolors.WARNING, 'test w', bcolors.ENDC
# print bcolors.FAIL, 'test w', bcolors.ENDC

argLen = len(sys.argv)
if argLen == 2:
    message = sys.argv[1]
    print bcolors.OKGREEN, 'git add .', bcolors.ENDC
    os.system('git add .')

    print bcolors.OKGREEN, 'git commit -m "' + str(message) + '"', bcolors.ENDC
    os.system('git commit -m "' + str(message) + '"')

    print bcolors.OKGREEN, 'git push origin master', bcolors.ENDC
    os.system('git push origin master')

else:
    print bcolors.FAIL, 'Please specify commit message.' , bcolors.ENDC
