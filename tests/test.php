<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Stec
 * Date: 27.05.2017
 * Time: 14:08:00
 */
require __DIR__ . '/../vendor/autoload.php';

$containerBuilder = new \DI\ContainerBuilder();
$containerBuilder->addDefinitions([
    \Mugen\Request::class => new \Mugen\Request(),
    Test::class => DI\object()->constructor(DI\get('\Mugen\Request')),
]);
$container = $containerBuilder->build();

class Test
{

    /**
     * Test constructor.
     * @Inject
     * @param \Mugen\Request $request
     */
    public function __construct(\Mugen\Request $request)
    {
        print 'Test: '.$request->path();
    }
}

new Test();

